<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arDescription = array(
    "NAME"=>GetMessage("GD_PRODUCTS_NAME"),
    "DESCRIPTION"=>GetMessage("GD_PRODUCTS_DESCRIPTION"),
    "ICON"=> "",
    "GROUPS"=>array("ID"=>"personal"),
    "SU_ONLY"=>false,
    "SG_ONLY"=>false,
    "BLOG_ONLY"=>false,
    "AI_ONLY"=>false,
);
?>