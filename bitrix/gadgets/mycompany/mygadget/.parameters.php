<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if(!CModule::IncludeModule('form'))
    return false;

$arForms = array(""=> GetMessage("GD_PROPERTIES_EMPTY"));
$dbForm = CForm::GetList(
    $by="s_id", $order="desc",Array(), $is_filtered);
while ($arForm = $dbForm ->GetNext())
{
    $arForms[$arForm["ID"]] = "[" . $arForm["ID"]. "] ". $arForm["NAME"];
}


$arParameters = array(
    "PARAMETERS" => array(
        "FORM_ID"=> array(
            "NAME"=>GetMessage("GD_PRODUCTS_FORM_ID"),
            "TYPE"=>"LIST",
            "VALUES"=>$arForms,
            "MULTIPLE"=>"N",
            "DEFAULT"=>"",
            "REFRESH"=>"Y"
        ),
    ),
    "USER_PARAMETERS" => array(
        "ELEMENT_COUNT"=>array(
            "NAME"=>GetMessage("GD_PRODUCTS_FORM_COUNT"),
            "TYPE"=>"STRING",
            "DEFAULT"=>5,
          ),
        "LINK"=>array(
            "NAME"=>"Ссылка на список результатов",
            "TYPE"=>"LOCATION",
            "DEFAULT"=>"",

        )
    ),
);
?>