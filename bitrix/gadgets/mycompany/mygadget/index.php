<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//dump($arGadgetParams);
$RESULT_ID = 1;
if(!CModule::IncludeModule('form'))
    return false;

// ID результата
$arFilter = array();
$arFields = array();
$arFields[] = array();

$rsResults = CFormResult::GetList($RESULT_ID,
    ($by="s_timestamp"),
    ($order="desc"),
    $arFilter,
    $is_filtered,
    "Y",
    100);
while ($arForm1 = $rsResults ->GetNext())
{
    $tdRezume[]=$arForm1;
}
$today = date("d.m.Y");
//echo($today);
$tdRezumRes =array();
foreach ($tdRezume as $tdRez)
{
    if(date("d.m.Y",strtotime($tdRez["TIMESTAMP_X"]) == $today))
    {
        $tdRezumRes[]=$tdRez;
    }

}
?>
Кол-во резюме,  добавленных за сегодняшний день:
<?php
if(empty($arGadgetParams["LINK"])):
?>
<a href="/bitrix/admin/form_result_list.php?PAGEN_1=1&SIZEN_1=20&lang=ru&WEB_FORM_ID=<?echo $RESULT_ID?>&set_filter=Y&adm_filter_applied=0&find_date_create_1_FILTER_PERIOD=day&find_date_create_1_FILTER_DIRECTION=current&find_date_create_1=<?echo $today?>&find_date_create_2=<?echo $today?>">
    <?echo count($tdRezumRes);?>
    </br></a>
<?else:?>
<a href="<?echo($arGadgetParams["LINK"])?>">
    <?echo count($tdRezumRes);?>
    </br></a>
<?endif;?>

Всего резюме:<a href="https://emelyanov.ivsupport.ru/bitrix/admin/form_result_list.php?lang=ru&WEB_FORM_ID=<?echo $RESULT_ID?>"><?php
echo count($tdRezume);
?>
</a>