<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="sb_nav">
<ul>


    <?
foreach($arResult as $arItem):
if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
continue;
?>
    <?if($arItem["SELECTED"]):?>
    <li class="open current">
        <span class="sb_showchild"></span>
        <a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
        <ul>
            <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
        </ul>
    </li>
    <?else:?>

    <li class="close" ><a href="<?=$arItem["LINK"]?>" class="selected"><span><?=$arItem["TEXT"]?></span></a></li>

<?endif?>
<?endforeach?>
        </ul>
    </div>

