<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<script type="text/javascript" >
    $().ready(function(){
        $(function(){
            $('#slides').slides({
                preload: true,
                generateNextPrev: false,
                autoHeight: true,
                play: 4000,
                effect: 'fade'
            });
        });
    });
</script>

<div class="sl_slider" id="slides">
    <div class="slides_container">
        <?foreach($arResult["ITEMS"] as $arItem):?>
        <div>
                <img src="<?=$arItem["DETAIL_PICTURE"]["src"];?>" alt="" />
                <h2><a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?>"><?echo $arItem["NAME"];?></a></h2>
                <p><?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["NAME"];?> Весего за <?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["PROPERTY_PRICE_VALUE"];?> руб</p>
                <a href="<?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["DETAIL_PAGE_URL"];?>" class="sl_more" title="<?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["NAME"];?>">Подробнее &rarr;</a>
        </div>
        <?endforeach;?>
    </div>
</div>



