<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php
foreach ($arResult["ITEMS"] as $ID=>$arItems)
{
    $arImage=CFile::ResizeImageGet($arItems['DETAIL_PICTURE'], array('width'=>$arParams["LIST_PREV_PIC_W"], 'height'=>$arParams["LIST_PREV_PIC_H"]), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    $arResult["ITEMS"][$ID]['DETAIL_PICTURE']=$arImage;

}
?>
