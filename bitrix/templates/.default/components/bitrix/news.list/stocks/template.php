<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
<div class="sb_action">
    <a href="<?echo $arItem["DETAIL_PAGE_URL"];?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt=""/></a>
    <h4><?echo $arItem["NAME"];?></h4>
    <h5><a href=""><?echo $arItem["PREVIEW_TEXT"];?> <?=$arItem["PROPERTIES"]["PRICE"]["VALUE"];?></a></h5>
    <?if (count($arItem["DISPLAY_PROPERTIES"]['LINK']['LINK_ELEMENT_VALUE'])):
        $linkValues = $arItem["DISPLAY_PROPERTIES"]['LINK']['LINK_ELEMENT_VALUE'];
		$linkVal = array_shift($linkValues);
        ?>
        <a href="<?=$linkVal['DETAIL_PAGE_URL']?>" class="sb_action_more">Подробнее &rarr;</a><a href="<?=$linkVal['DETAIL_PAGE_URL']?>" class="sb_action_more">Подробнее &rarr;</a>

    <?endif;?>
</div>
<?endforeach;?>