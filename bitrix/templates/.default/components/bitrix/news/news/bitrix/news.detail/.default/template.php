<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?php
$this->SetViewTarget("news_detail_date");?>

<span class="main_date"><?=$arResult["DISPLAY_ACTIVE_FROM"];?></span>
<?php $this->EndViewTarget("news_detail_date");
?>
<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" align="left" alt=""/>
<?echo $arResult["DETAIL_TEXT"];?>
<div style="clear:both"></div>
<?if($arResult['AUTHOR']):?>
Автор: <?=$arResult['AUTHOR'][0]?>
<?endif;?>