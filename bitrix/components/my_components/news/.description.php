<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("IBLOCK_VACANCIES_NAME"),
	"DESCRIPTION" => GetMessage("IBLOCK_VACANCIES_DESCRIPTION"),
	"ICON" => "/images/news_all.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "news",
			"NAME" => GetMessage("T_IBLOCK_DESC_VACANCIES"),
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "vacancies_cmpx",
			),
		),
	),
);

?>