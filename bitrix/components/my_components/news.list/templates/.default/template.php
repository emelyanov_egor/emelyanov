<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php
$arSelect = array("NAME", "ID", "ELEMENT_CNT");
$arFilter = Array('IBLOCK_ID'=>11);
$db_list = CIBlockSection::GetList(Array(), $arFilter, true, $arSelect);
while($ar_result = $db_list->GetNext())
{
    $section[]= $ar_result;
}
?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
    <div class="info">
    <?foreach($section as $sec):?>
    <div ><?=$sec["NAME"];?> (<?=$sec["ELEMENT_CNT"];?>)</div>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?if ($sec["ID"] == $arItem["IBLOCK_SECTION_ID"]):?>
                <?
                // кнопки эрмитажа
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <p><?echo $arItem["NAME"]?></p>
                <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">Подробнее</a><br />
                </div>
            <?endif?>
        <?endforeach;?>
    <?endforeach;?>
    </div>
    <div class="info" style="display: none"></div>
<script>
    $( ".title" ).click(function() {
        $( ".info" ).toggle();
    });
</script>