<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/constants.php");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("CIBlockHandler", "OnBeforeIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("CIBlockHandler", "OnBeforeIBlockElementDeleteHandler"));
AddEventHandler("main", "OnBeforeUserUpdate", Array("CMainHandler", "OnBeforeUserUpdateHandler"));
class CMainHandler
{
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        $group_new= 0;
        $group_old= 0;
        $res = CUser::GetUserGroupList($arFields["ID"]);
        while ($arGroup = $res->Fetch()){
            $IDS_old[]= $arGroup;
        }
        foreach ($IDS_old as $ids_old)// просматриваем старые group id
        {
            if($ids_old["GROUP_ID"] == CONTENT_EDITOR_ID)// если был редактором
            {
                $group_old = 1;// подымаем флаг
            }

        }
        foreach ($arFields["GROUP_ID"] as $ids_new)//просматриваем новые group id
        {
            if($ids_new["GROUP_ID"] == CONTENT_EDITOR_ID)// если стал редактором
            {
                $group_new = 1;// подымаем флаг
            }

        }
        if ($group_new == 1 && $group_old == 0 )//если до этого не был редактором до этого и вдруг стал
        {

            //достаем emailы контент редакторов
            $userParams = array(
                'FIELDS' => array("EMAIL")
            );
            $res = CUser::GetList(($by="ID"), ($order="ASC"),
                array(
                    'GROUPS_ID' => CONTENT_EDITOR_ID,
                ), $userParams);
            while($emUser = $res->Fetch())
            {
                $ContentEditorEmail[]= $emUser["EMAIL"];
            }
            foreach($ContentEditorEmail as $key => $item)
            {
                if ($item == $arFields["EMAIL"])
                {
                    unset($ContentEditorEmail[$key]);//уберем email нового редактора
                }
            }
            // если emailы есть то делаем рассылку
            if(count($ContentEditorEmail)>0)
            {
                //dump($arFields["NAME"], true);
                $arEventFields = array(
                    "TEXT" => $arFields["NAME"] . " " . $arFields["LAST_NAME"] ,
                    "EMAIL" => implode(", ", $ContentEditorEmail),
                );

                CEvent::Send("NEW_CONTENT_EDITOR", "s1", $arEventFields);// отправляем сообщение
                CEventLog::Add(array(
                    "SEVERITY" => "SECURITY",
                    "AUDIT_TYPE_ID" => "NEW_CONTENT_EDITER",
                    "MODULE_ID" => "main",
                    "ITEM_ID" => "",
                    "DESCRIPTION" => "добавлен новый контент редактор: " . $arFields["NAME"] . " " . $arFields["LAST_NAME"],
                ));// добавляем в журнал
            }
        }
    }
}

class CIBlockHandler
{
    // создаем обработчик события "OnBeforeIBlockElementUpdate"
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        $result = time() - strtotime($arFields["ACTIVE_FROM"]);

        //dump($arFields, true);
        if($result<259200 && $arFields["ACTIVE"] == "N" && $arFields["IBLOCK_ID"] == 1 )
        {

            global $APPLICATION;
            $APPLICATION->throwException("вы деактивировали свежую новость!");
            $arFields["ACTIVE"] = "Y";
            return false;
        }

    }

   function OnBeforeIBlockElementDeleteHandler($ID)
   {
       $arSelect = array("ID", "NAME", "DATE_ACTIVE_FROM", "SHOW_COUNTER", "ACTIVE");
       $arFilter = array("IBLOCK_ID" => 2, "ID" => $ID);
       $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);

        while($ob = $res->GetNextElement()) {
            $arField = $ob->GetFields();
        }
        //dump($arFields, true);

       if($arField["SHOW_COUNTER"]>1)
       {
           $arFields["ACTIVE"] = "N";
           global $APPLICATION;
           $APPLICATION->throwException("Элемент нельзя удалить. Кол-во просмотов: "  . $arField["SHOW_COUNTER"]);
           return false;

       }


   }




}
?>