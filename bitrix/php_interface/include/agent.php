<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/constants.php");
function AgentCheckSale()
{
    if(CModule::IncludeModule("iblock")) {
        $arSelect = array("ID", "NAME", "DATE_ACTIVE_FROM");
        $arFilter = array("IBLOCK_ID" => CATALOG_IBOCK_ID, "!ACTIVE_DATE" => "Y");
        $res = CIBlockElement::GetList(
            array(),
            $arFilter,
            false,
            array("nPageSize" => 50),
            $arSelect);
        while ($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arFinishedSale[]= $arFields;

        }
        if(count($arFinishedSale) > 0)
        {
            CEventLog::Add(array(
                "SEVERITY" => "SECURITY",
                "AUDIT_TYPE_ID" => "FINISHED_SALE",
                "MODULE_ID" => "iblock",
                "ITEM_ID" => "",
                "DESCRIPTION" => "Закончилось ".count($arFinishedSale)." акций",
            ));



            $arFilter = Array(
                "GROUPS_ID" => Array(GROUP_ADMIN_ID)
            );
            $rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
            $arAdminEmail = array();
            while($arResUser = $rsUsers->GetNext())
            {
                $arAdminEmail[] = $arResUser["EMAIL"];
            }

            if(count($arAdminEmail)>0)
            {
                $arEventFields = array(
                    "TEXT" => count($arFinishedSale),
                    "EMAIL" => implode(", ", $arAdminEmail),
                );
                CEvent::Send("FINISHED_SALE", SITE_ID, $arEventFields);
                //dump($arEventFields, true);
            }
        }
    }
    return "AgentCheckSale();";
}
?>
